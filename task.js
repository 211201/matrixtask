var problem = [
    [">", "-", "-", "-", "A", "-", "@", "-", "+"],
    [" ", " ", " ", " ", " ", " ", " ", " ", "|"],
    ["+", "-", "U", "-", "+", " ", " ", " ", "C"],
    ["|", " ", " ", " ", "|", " ", " ", " ", "|"],
    ["s", " ", " ", " ", "C", "-", "-", "-", "+"],
];

const directions = {
    right: { row: 0, col: 1 },
    left: { row: 0, col: -1 },
    down: { row: 1, col: 0 },
    up: { row: -1, col: 0 }
};

var locateFirst = function (m) {
    for (var i = 0; i < m.length; i++) {
        for (var j = 0; j < m[i].length; j++) {
            if (m[i][j] === ">") {
                return { x: i, y: j };
            }
        }
    }
    return null;
};

function isChar(char) {
    const chars = ["+", "-", "|", "@"];
    return chars.indexOf(char) !== -1 || (char >= "A" && char <= "Z");
}

function getDirection(problem, row, col, currentDirection) {
    const possibleDirections = [directions.up, directions.down, directions.left, directions.right];
    const oppositeDirections = {
        right: directions.left,
        left: directions.right,
        down: directions.up,
        up: directions.down
    };

    for (let i = 0; i < possibleDirections.length; i++) {
        const newDirection = possibleDirections[i];
        const newRow = row + newDirection.row;
        const newCol = col + newDirection.col;

        if (
            !(newDirection.row === -currentDirection.row && newDirection.col === -currentDirection.col) &&
            newRow >= 0 && newRow < problem.length &&
            newCol >= 0 && newCol < problem[0].length &&
            problem[newRow][newCol] !== ' '
        ) {
            return newDirection;
        }
    }

    return currentDirection;
}

function Output(problem) {
    const start = locateFirst(problem);
    let row = start.x;
    let col = start.y;
    let currentDirection = directions.right;
    let path = '';
    let letters = '';

    while (true) {
        const char = problem[row][col];
        path += char;

        if (char >= 'A' && char <= 'Z') {
            letters += char;
        }

        if (char === 's') {
            break;
        }

        const nextRow = row + currentDirection.row;
        const nextCol = col + currentDirection.col;

        if (nextRow >= 0 && nextRow < problem.length && nextCol >= 0 && nextCol < problem[0].length && problem[nextRow][nextCol] !== ' ') {
            row = nextRow;
            col = nextCol;
        } else {
            currentDirection = getDirection(problem, row, col, currentDirection);
            row += currentDirection.row;
            col += currentDirection.col;
        }
    }

    return { path, letters };
}

var result = Output(problem);
console.log("Path:", result.path);
console.log("Letters:", result.letters);
